function getUser(userID, callback) {
	/* This is a hypothetical function. Normally we'd talk to a database or so, but this is to illustrate the concept... */
	var someUser = {name: "Joe Bloggs", age: 42};
	callback(someUser); /* Here we call the callback from the second function argument, with our fake user */
}

/* Code: */
getUser(42, function(user){
	/* Now `user` contains {name: "Joe Bloggs", age: 42} */
});

/* We could also write this as: */
function whenDone(user) {
	/* Now `user` contains {name: "Joe Bloggs", age: 42} */
}

getUser(42, whenDone);